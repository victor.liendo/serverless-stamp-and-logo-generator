# Serverless Typescript Template

## Introduction

This is a basic template, holding only the basic setup of a project that integrates serverless and typescript. Some of the fixtures of this template is:

- Updates
- Typescript
- Eslint
- Prettier
- Test (mocha, chai and sinon)
- Serverless

## New Updates from template

```shell
# to add new repository
git remote add template https://github.com/leypal/leypal-ms-template.git  (https version)
git remote add template git@github.com:/leypal/leypal-ms-template.git     (ssh version)

# get new updates from template repository
git pull template master
```


## Install dependencies

```shell
npm i
# or
yarn install
```

## Run the api

```shell
npm run start
# or
yarn start
```

## Run a function

```shell
npm run dev [function]
# or
yarn dev [function]
```

## Run tests

```shell
npm run test
# or
yarn test
```

## Manually deploy with cloudformation

```shell
npm run deploy-dev
# or
yarn deploy-dev
```

## Manually deploy only one function (upload zip file)
```shell
serverless deploy function -f functionName
```

Note: https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy-function/


By default it will be searching for tests to run ( **\*.spec.ts** files) in the path `tests/**/*`, you can add paths to the command above.

It also will check the style of the code on those paths and try to fixed, if it can't will display an error or a warning depending on the error

## Run eslint to fix style

```shell
npm run lint
# or
yarn lint
```

By default it will lint the files in the paths `src/**/*` and `tests/**/*`, you can add paths to the command above.


## Changes after clone

in serverles.yml

```yml
service:
  name:  leypal-ms-<name-of-function> #change name
  ...
  module: <name-module> #this name will be part of path to module ej. domain.com/<module>/<action>
``` 

## API GATEWAY
### Intregrate new lambda function to this gateway

add/change this in serverless.yml

```yml
provider:
...
  apiGateway:
    restApiId:
      'Fn::ImportValue': LeyPalApiGateway-restApiId
    restApiRootResourceId:
      'Fn::ImportValue': LeyPalApiGateway-rootResourceId
    websocketApiId:
      'Fn::ImportValue': LeyApiGateway-websocketApiId
...      
```

Should be change models name in documentation node

```yml
custom:
  ...
  documentation:
  ...
    models:
       name: 'Name of model'  #must be a unique name  

```

More information:

- [Serverless Swagger API Documentation](https://github.com/deliveryhero/serverless-aws-documentation/blob/master/README.md)
- [Serveless API DOcumentation](https://serverless.com/plugins/serverless-aws-documentation/)
