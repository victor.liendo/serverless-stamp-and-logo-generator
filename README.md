# Overview

This microservice was developed for a Serverless NodeJS Backend which, during User Signup, sends some information to a AWS SQS Queue, from which the message is taken and then logos and stamps for the User and Company are generated and saved in Amazon S3

# Service documentation template
General information about the service
Github [repository]

  https://github.com/leypal/leypal-ms-helper-images.git

Functions in this service take an incoming JSON payload
contaning some key attributes of Users and Companies, and
create blank images with some text over them, to be used
as SIGNATURE, STAMP and LOGO

# Table of contents
1. [Service Invoke](#service-invoke)
2. [Input/Output](#input-output)
    1. [Input](#input)
    2. [Output](#output)
3. [Required Infrastructure](#infra)
    1. [Environment Variables](#infra-env)
    2. [Database](#infra-database)
    3. [Layers](#infra-layers)
    4. [Others](#infra-others)
4. [Deploy](#deploy)
5. [TO-DO](#to-do)
6. [Additional Info](#additional-info)


<div id='service-invoke'/>

# Service invocation
- functions in this service are invoqued through sending
events to an SQS QUEUE (ActionsImageCreationQueue.Arn 
in serverless.yml)

<div id='input-output'/>

# Input/Output

<div id='input'/>

## Input

Events are of the type :

{
		    "notificationType": "COMPANY_CREATED",
		    "notificationSubType": "company",
		    "notificationUserId": 23,
		    "notificationCompanyId": 16,
		    "data": {
				"company": { 
					"id": 16,
					"name": "LeyPal INC",
					"nifCifVal":"0019929919",
					"address": "Av Sergio Ramos, calle Carles Puyol, nro. 38",
					"city":"Madrid",
					"country":"España",
					"zipCode":"0010-010029",
					"phone":"+59891991932"
				}
		    } 
}

OR

{
		    "notificationType": "ACCOUNT_CREATED",
		    "notificationSubType": "user",
		    "notificationUserId": 3,
		    "notificationCompanyId": 3,
		    "data": {
				"user": { 
						   "id": "3",
						   "companyId": "3",
						   "name": "ANGEL GARRIDO"
						}
		    }
}


<div id='output'/>

## Output
  - Import a HTML file and watch it magically convert to Markdown
  - Drag and drop images (requires your Dropbox account be linked)

Two kinds of outputs are produced:

1. A message in sent to a different queue, to be processed at a later stage (cff-sqs-NotificationsEventsQueueURL)

{
                notificationType: OutputEventType.SIGNATURE,
                notificationSubType: "user",
                notificationUserId: userId,
                notificationCompanyId: companyId,
                data: {
                            user: { 
                                id: userId,
                                signatureUrl: s3Key
                            }
                }
            
}

{
                    notificationType: OutputEventType.STAMP,
                    notificationSubType: "company",
                    notificationUserId: companyId,
                    notificationCompanyId: companyId,
                    data: {
                            company: { 
                                id: companyId,
                                stampUrl: s3Key
                            }
                    }
                
}

{
            notificationType: OutputEventType.LOGO,
            notificationSubType: "company",
            notificationUserId: companyId,
            notificationCompanyId: companyId,
            data: {
                        company: { 
                            id: companyId,
                            logoUrl: s3Key
                        }
            }
}

with OutputEvenType being one of:

{
    STAMP = 'COMPANY_STAMP_FILE_CREATED',
    LOGO = 'COMPANY_LOGO_FILE_CREATED',
    SIGNATURE = 'ACCOUNT_SIGNATURE_FILE_CREATED'
}

2. An image is uploaded to cff-s3-LeypalUsersBucket

<div id='infra'/>

# Service required infrastructure

<div id='infra-env'/>

## Environment Variables
| Plugin | README |
| ------ | ------ |
| Dropbox | [plugins/dropbox/README.md][PlDb] |
| GitHub | [plugins/github/README.md][PlGh] |
| Google Drive | [plugins/googledrive/README.md][PlGd] |
| OneDrive | [plugins/onedrive/README.md][PlOd] |
| Medium | [plugins/medium/README.md][PlMe] |
| Google Analytics | [plugins/googleanalytics/README.md][PlGa] |

S3_BUCKET_USERS: !ImportValue cff-s3-LeypalUsersBucket
    DEBUG: 1
# Output Queue for User SIGNATURE images
AWS_SQS_USERS_SIGNATURE_OUTPUT_QUEUE_URL: !ImportValue cff-sqs-NotificationsEventsQueueURL
# Output Queues for Company LOGO and STAMP
AWS_SQS_COMPANIES_STAMP_OUTPUT_QUEUE_URL: !ImportValue cff-sqs-NotificationsEventsQueueURL
AWS_SQS_COMPANIES_LOGO_OUTPUT_QUEUE_URL:  !ImportValue cff-sqs-NotificationsEventsQueueURL
# width,height for the output images (USER)
USER_SIGNATURE_DIMENSIONS: '400x120'
USER_SIGNATURE_FONTS: 'Satisfy-Regular'
# width, height for the output images (COMPANY)
COMPANY_STAMP_DIMENSIONS: '400x120'
COMPANY_LOGO_DIMENSIONS: '300x120'
COMPANY_STAMP_LOGO_FONTS: 'Alata-Regular'


<div id='infra-database'/>

## Database

<div id='infra-layers'/>

## Layers

An external dependency (canvas) is available in the AWS environment

<div id='infra-others'/>

## Others
First Tab:
```sh
$ node app
```


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. 

   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [sqs-events-queue]: <https://google.com>

<div id='deploy'/>

# Deploy
```bash
export AWS_ACCESS_kEY_ID=${aws_access_key_id}
export AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}
docker-compose run node bash
npm install
sls deploy --stage prod
```

DEPLOY IS BEING DONE BY ISSUING:

$ serverless deploy --stage prod

serverless 
<div id='to-do'/>

# TO-DO

 - Write MORE Tests
 - Add Night Mode
 - There should be a way of computing the width based
   on the larger text that is being written in the image

<div id='additional-info'/>

# Information

- [Template](./template.README.md)
