/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable prettier/prettier */

import { 
    SQSEvent,
    APIGatewayProxyResult,
    Context
} from 'aws-lambda';

import {
    defaultErrorHandlerDecorator,
} from '../../utils/error'
import {
  createSignatureImage,
  createStampImage,
  createLogoImage
} from '../../services/create-image-service'

/*
    Generates a signature image to be uploaded in AWS S3
*/
export async function sqsEventHandler (event: SQSEvent, context: Context): Promise<APIGatewayProxyResult> {
    return defaultErrorHandlerDecorator(async (event: SQSEvent) => {
      /*process input event*/
      const messageProcessed=[]
      for (const message of event.Records) {
        // Getting the Uset attributes
        const messageBody=JSON.parse(message.body);
        switch (messageBody.type.toUpperCase())  {
          case 'ACCOUNT_CREATED' : {
            console.info("ACCOUNT CREATED EVENT RECEIVED")
            messageProcessed.push(await createSignatureImage(messageBody, context))
            break
          }
          case 'COMPANY_CREATED' : {
            console.info("COMPANY CREATED EVENT RECEIVED")
            messageProcessed.push(await createStampImage(messageBody, context))
            messageProcessed.push(await createLogoImage(messageBody, context))
            break           
          }
          default : {
            console.info("UNKNOWN EVENT RECEIVED")
            break
          }
        }     
      }
      return {
        statusCode: 200,
        body: JSON.stringify(
          {
            ...messageProcessed
          },
          null,
          2
        )
      }
    })(event)
}