export default interface EventTypeModel<T, ST> {
  type: T
  subtype: ST
}

export type ACCOUNT_CREATED = EventTypeModel<
  'ACCOUNT_CREATED',
  'ADMIN' | 'API' | 'USER'
>
export type ACCOUNT_UPDATED = EventTypeModel<'ACCOUNT_UPDATED', undefined>
export type ACCOUNT_LOGIN = EventTypeModel<'ACCOUNT_LOGIN', undefined>
export type ACCOUNT_RESET_PASSWORD = EventTypeModel<
  'ACCOUNT_RESET_PASSWORD',
  undefined
>
export type ACCOUNT_CHANGE_PASSWORD = EventTypeModel<
  'ACCOUNT_CHANGE_PASSWORD',
  'USER' | 'ADMIN' | 'reset_password'
>
export type COMPANY_CREATED = EventTypeModel<'COMPANY_CREATED', undefined>
