export enum TransactionType {
  ACTION = 'ACTION',
  EVENT = 'EVENT'
}

export enum OutputEventType {
  STAMP = 'COMPANY_STAMP_FILE_CREATED',
  LOGO = 'COMPANY_LOGO_FILE_CREATED',
  SIGNATURE = 'ACCOUNT_SIGNATURE_FILE_CREATED'
}

import {
  CreateOptions,
  FindOptions,
  Model,
  UpdateOptions
} from 'sequelize/types'
import EventTypeModel, * as EventTypes from './event-types'
export interface SequelizeSetUp {
  database: string
  username: string
  password: string
  host: string
  port?: number
  dialect: 'mysql' | 'postgres' | 'mssql' | 'sqlite' | 'mariadb'
  dialectOptions: { connectTimeout: number }
  dialectModule: object | undefined
}

export interface Read<T> {
  findAll: (options?: FindOptions) => Promise<T[]>
  findOne: (options?: FindOptions) => Promise<T | null>
}

export interface Create<T> {
  create: (data: T, options?: CreateOptions) => Promise<T>
}

export interface Delete {
  delete: (id: string | number) => Promise<number>
}

export interface Update<T> {
  update: (data: T, options: UpdateOptions) => Promise<T>
}

export interface BasicRepository<T>
  extends Read<T>,
    Create<T>,
    Update<T>,
    Delete {
  model: typeof Model
}

export enum Language {
  EN = 'en',
  ES = 'es'
}

export const loginSchema = {
  name: { type: 'string' },
  password: { type: 'string' }
}

export interface LoginIncomingRequest {
  username: string
  password: string
}
export class LoginRequestModel implements ValidatorModel<LoginIncomingRequest> {
  constructor(
    public username: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public password: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    }
  ) {}
}
export interface ResetPasswordRequest {
  token: string
  password: string
}
export class ResetPasswordRequestModel
  implements ValidatorModel<ResetPasswordRequest> {
  constructor(
    public token: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public password: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    }
  ) {}
}

export enum UserType {
  ADMIN = 'ADMIN',
  USER = 'USER',
  API = 'API',
  ROOT = 'ROOT'
}

export const enum PaymentMethodsType {
  PAYTPV = 'PAYTPV',
  DIRECTDEBIT = 'DIRECTDEBIT'
}

export interface CompanyAttributes extends DatabaseModel {
  id?: number
  globalId?: string
  nifCif?: string
  name?: string
  taxId?: string
  country?: number
  state?: string
  address?: string
  phone?: string
  city?: string
  url?: string
  zipCode?: string
  particular?: boolean
  image?: string
  stamp?: string
  roi: boolean
  defaultCard?: number
  domain?: string
  plan?: number
  paymentMethod?: PaymentMethodsType
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date

  Users?: KeyPresent<UserAttributes, 'id'>[]
  Order?: KeyPresent<OrderAttributes, 'id'>
  Country?: KeyPresent<CountryAttributes, 'id'>
}

export interface DatabaseModel {
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
}

export interface UserAttributes extends DatabaseModel {
  id?: number
  globalId?: number
  companyId: number
  type: UserType
  email: string
  password?: string
  name?: string
  lastName?: string
  language?: Language
  draft?: boolean
  disabled?: boolean
  disabledByUser?: boolean
  emailConfirmed?: boolean
  emailNotification?: boolean
  useNewsLetter?: boolean
  signatureImage?: string
  defaultCard?: number
  firstTime?: boolean
  confirmationToken?: string
  tokenDueDate?: Date
  resetPasswordToken?: string
  resetPasswordTokenDueDate?: Date
  webhookUrl?: string
  disabledWebhook?: boolean
  disabledWebhookReason?: string
  disabledWebhookByUser?: number
  apiKey?: string
  apiSecret?: string
}
export interface OrderPeriodAttributes extends DatabaseModel {
  id?: number
  order: number
  startDate: Date
  expirationDate?: Date
  usedCredits?: boolean
  usedTemplates?: boolean
  usedAdvancedSignatures?: boolean
  usedFastSignatures?: boolean
  usedEmailSignatures?: boolean
  usedInPersonSignatures?: boolean
  usedSelfSignatures?: boolean
}

export interface OrderPeriodStatusAttributes extends DatabaseModel {
  id?: number
  orderPeriod: number
  user: number
  usedCredits?: number
  usedTemplates?: number
  usedAdvancedSignatures?: number
  usedFastSignatures?: number
  usedEmailSignatures?: number
  usedInPersonSignatures?: number
  usedSelfSignatures?: number
}
export interface OrderAttributes extends DatabaseModel {
  id?: number
  paymentMethod?: PaymentMethodsType
  coin?: string
  total?: number
  totalEuros?: number
  company: number
  product: number
  nroProduct: number
  startDate: Date
  orderProduct: number
  expiration: Date
  vat: number
  priceBase: number
  cancelled: boolean

  OrderProduct?: KeyPresent<OrderProductAttributes, 'id' | 'api' | 'users'>
}
export interface OrderProductAttributes extends DatabaseModel {
  id?: number
  name?: string
  users?: number
  api?: boolean
}
export interface UserExtras {
  companyId: number
  draft: boolean
  disabled: boolean
  disabledByUser: boolean
  emailConfirmed: boolean
  firstTime: boolean
}
export interface UserCreate {
  type: UserType
  email: string
  password: string
  name: string
  lastName: string
  language: Language
  emailNotification: boolean
  useNewsLetter: boolean
  signatureImage: string
  apiKey?: string
  apiSecret?: string
  company: {
    nifCif: string
    name: string
    taxId: string
    country?: number
    state: string
    address: string
    phone: string
    city: string
    url: string
    zipCode: string
    particular: boolean
    image: string
    stamp: string
    roi: boolean
    paymentMethod: PaymentMethodsType
  }
}

export interface PropertyToValidate<T, R extends boolean> {
  type: 'string' | 'number' | 'boolean' | 'Function' | 'array' | 'object'
  required: R
  default: R extends true ? T | undefined : T
}
export type ValidatorModel<T> = {
  [P in keyof T]: PropertyToValidate<any, any>
}

export type UserCreateModel =
  | UserTypeAPICreateRequest
  | UserTypeUSERCreateRequest
  | UserTypeADMINCreateRequest
export interface UserTypeAPICreateRequest {
  type: UserType.API
  email: string
  webhookUrl?: string
  language?: Language
  emailNotification?: boolean
}
export class UserTypeAPICreateRequestModel
  implements ValidatorModel<UserTypeAPICreateRequest> {
  constructor(
    public type: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public email: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public webhookUrl: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    },
    public language: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    },
    public emailNotification: PropertyToValidate<boolean, false> = {
      type: 'boolean',
      required: false,
      default: true
    }
  ) {}
}
export interface UserTypeUSERCreateRequest {
  type: UserType.USER
  name: string
  lastName: string
  email: string
  language?: Language
  password?: string
}
export class UserTypeUSERCreateRequestModel
  implements ValidatorModel<UserTypeUSERCreateRequest> {
  constructor(
    public type: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public name: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public lastName: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public email: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public language: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    },
    public password: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    }
  ) {}
}
export interface UserTypeADMINCreateRequest {
  type: UserType.ADMIN
  password: string
  name: string
  lastName: string
  emailNotification: boolean
  useNewsLetter: boolean
  signatureImage?: string
  email: string
  language?: Language
  company: {
    nifCif?: string
    name: string
    taxId: string
    country: number
    state: string
    address: string
    phone: string
    city: string
    url: string
    zipCode: string
    particular: boolean
    image?: string
    stamp?: string
    roi: boolean
    paymentMethod?: PaymentMethodsType
  }
}

export class UserTypeADMINCreateRequestModel
  implements ValidatorModel<UserTypeADMINCreateRequest> {
  constructor(
    public type: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public password: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public name: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public lastName: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public emailNotification: PropertyToValidate<boolean, true> = {
      type: 'boolean',
      required: true,
      default: undefined
    },
    public useNewsLetter: PropertyToValidate<boolean, true> = {
      type: 'boolean',
      required: true,
      default: undefined
    },
    public signatureImage: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    },
    public email: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public language: PropertyToValidate<string, false> = {
      type: 'string',
      required: false,
      default: ''
    },
    public company: PropertyToValidate<
      {
        nifCif: PropertyToValidate<string, false>
        name: PropertyToValidate<string, true>
        taxId: PropertyToValidate<string, true>
        country: PropertyToValidate<number, true>
        state: PropertyToValidate<string, true>
        address: PropertyToValidate<string, true>
        phone: PropertyToValidate<string, true>
        city: PropertyToValidate<string, true>
        url: PropertyToValidate<string, true>
        zipCode: PropertyToValidate<string, true>
        particular: PropertyToValidate<boolean, true>
        image: PropertyToValidate<string, false>
        stamp: PropertyToValidate<string, false>
        roi: PropertyToValidate<boolean, true>
        paymentMethod: PropertyToValidate<string, false>
      },
      true
    > = {
      type: 'object',
      required: true,
      default: {
        nifCif: {
          type: 'string',
          required: false,
          default: ''
        },
        name: {
          type: 'string',
          required: true,
          default: undefined
        },
        taxId: {
          type: 'string',
          required: true,
          default: undefined
        },
        country: {
          type: 'number',
          required: true,
          default: undefined
        },
        state: {
          type: 'string',
          required: true,
          default: undefined
        },
        address: {
          type: 'string',
          required: true,
          default: undefined
        },
        phone: {
          type: 'string',
          required: true,
          default: undefined
        },
        city: {
          type: 'string',
          required: true,
          default: undefined
        },
        url: {
          type: 'string',
          required: true,
          default: undefined
        },
        zipCode: {
          type: 'string',
          required: true,
          default: undefined
        },
        particular: {
          type: 'boolean',
          required: true,
          default: undefined
        },
        image: {
          type: 'string',
          required: false,
          default: ''
        },
        stamp: {
          type: 'string',
          required: false,
          default: ''
        },
        roi: {
          type: 'boolean',
          required: true,
          default: undefined
        },
        paymentMethod: {
          type: 'string',
          required: false,
          default: ''
        }
      }
    }
  ) {}
}

export interface CountryAttributes extends DatabaseModel {
  id?: number
  vat?: number
  ue?: boolean
}

export interface BuyOrderRequest {
  product: number
  quantity: number
  activation: 'now' | 'final-period'
  paymentMethod: PaymentMethodsType
  companyId: number
}
export interface AuthorizerResult {
  user: string
  company: string
  token: string
  creation: number
}
export interface ErrorHandler<E extends Error, T> {
  type: { new (...params: any[]): E }
  handler: (error: E) => T
}
export interface ChangePasswordRequest {
  oldPassword: string
  newPassword: string
}
export class ChangePasswordRequestModel
  implements ValidatorModel<ChangePasswordRequest> {
  constructor(
    public oldPassword: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    },
    public newPassword: PropertyToValidate<string, true> = {
      type: 'string',
      required: true,
      default: undefined
    }
  ) {}
}

export interface RefreshTokenRequest {
  refreshToken?: string
}
export type KeyPresent<T, P extends keyof T> = T & { [K in P]-?: T[P] }
export type RemovedKeys<T, K extends keyof T> = T & { [P in K]?: never }
export type UserResponseModel = KeyPresent<
  RemovedKeys<
    UserAttributes,
    | 'password'
    | 'tokenDueDate'
    | 'confirmationToken'
    | 'resetPasswordToken'
    | 'resetPasswordTokenDueDate'
    | 'globalId'
  >,
  'id'
>
export type EventRequest<
  E extends EventTypeModel<any, any>,
  D extends EventRequestData
> = {
  status: EventStatus
  data: D
  notificationUserId: number
  notificationCompanyId: number
} & Pick<E, 'type' | 'subtype'>

export enum EventStatus {
  COMPLETED = 'completed',
  REJECTED = 'rejected',
  SENT = 'sent'
}
export type EventRequestData = {}
export interface AccountCreatedEvent extends EventRequestData {
  user: Required<Pick<UserAttributes, 'name' | 'email'> & { lastName: string }>
  password: string
}
export interface AccountUpdatedEvent extends EventRequestData {
  previousUser: UserResponseModel
  newUser: UserResponseModel
}
export interface AccountLoginEvent extends EventRequestData {
  userId: number
}
export interface AccountChangePasswordEvent extends EventRequestData {
  user: Required<
    Pick<UserAttributes, 'name' | 'email'> & {
      lastName: string
      token: string
      tokenDueDate: Date
    }
  >
}
export interface Session {
  readonly user: KeyPresent<
    UserAttributes,
    | 'id'
    | 'name'
    | 'lastName'
    | 'resetPasswordToken'
    | 'resetPasswordTokenDueDate'
  >
  readonly company: KeyPresent<CompanyAttributes, 'id'>
  readonly isAdmin: boolean
  readonly isUser: boolean
  readonly isAPI: boolean
  readonly isRoot: boolean
  readonly ip: string
  readonly isLogged: boolean
}
export interface CompanyCreatedEvent extends EventRequestData {
  company: KeyPresent<CompanyAttributes, 'id'>
}
export { EventTypes }
