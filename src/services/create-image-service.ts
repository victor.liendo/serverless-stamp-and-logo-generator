import {
  getSignatureImageDimensions,
  getStampImageDimensions,
  getLogoImageDimensions,
  getS3UsersBucketName,
  getFontsFamily,
  getUserSignatureGenerationOuputQueueURL,
  getCompanyStampGenerationOuputQueueURL,
  getCompanyLogoGenerationOuputQueueURL
 } from '../utils/env-variables';

import { Context } from 'aws-lambda';
import { TransactionType, OutputEventType } from '../../src/@types'

import {
  sendOutputMessage,
  getNextVerticalAxisPosition
} from '../utils'
  
const aws = require('aws-sdk');

export async function createSignatureImage (body: any, context: Context): Promise<any> {
    console.info('CREATING USER SIGNATURE IMAGE')
    try {
        // Import the required libraries or packages
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { createCanvas } = require('canvas')
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { registerFont } = require('canvas')
        // Setting the FONT FAMILY
        registerFont('src/resources/' + getFontsFamily('signature') +'.ttf', { family: 'FontName' })
        // Start with the image creation
        const imgDimensions = getSignatureImageDimensions().split('x');
        const width: number = Number.parseInt(imgDimensions[0]);
        const height: number = Number.parseInt(imgDimensions[1]);
        const messageBody=body
        const userId: number=Number.parseInt(messageBody.data.user.id);
        const companyId: number=Number.parseInt(messageBody.data.user.companyId);
        const userName: string=messageBody.data.user.name;
        const canvas = createCanvas(width, height);
        const ctx = canvas.getContext('2d');
        // Set some styles (Black background)
        ctx.fillStyle = '#fff';
        // Here we create a rectangle
        ctx.fillRect(0,0,width,height);
        // Now we fill the rectangle with the name coming in the event payload
        // and set some styles (black text)
        ctx.font = '20px FontName';
        ctx.textAlign = 'center';
        ctx.fillStyle = '#000';
        ctx.fillText(userName, width/2, height/2);
        // Lastly, let's get a buffer from the canvas
        const buffer=canvas.toBuffer("image/png");
        const s3Key = 'companies/' + companyId + 
                        '/users/' + userId + '/signature.png'
        // and save it to S3
        const uploadParams = {
            Bucket: getS3UsersBucketName(),
            Body: buffer,
            Key: s3Key
        } 
        const s3 = new aws.S3()
        const uploadResults = await s3.upload(uploadParams).promise().then(function (_data: any) {
            return ({status: 'OK', message:'Image generated and uploaded to S3 successfully !!!'})
        }, function (error: any) {
            return ({status: 'ERR', message: error})
        })
        if (uploadResults.status === 'OK') {
            const outputMessageBody = {
                type: OutputEventType.SIGNATURE,
                subtype: "user",
                notificationUserId: userId,
                notificationCompanyId: companyId,
                origin: context.functionName,
                notificationExecutionId: context.awsRequestId,
                transactionType: TransactionType.EVENT,
                data: {
                            user: { 
                                id: userId,
                                signatureUrl: s3Key
                            }
                }
            
            }
            const queueUrl = getUserSignatureGenerationOuputQueueURL();
            await sendOutputMessage(JSON.stringify(outputMessageBody), queueUrl);
            body.data['message']='Image created OK'
        }
        return body
    } catch (e) {
        console.info('Error', e)
        body.data['message']='Error while creating signature image ... ' + e
        return body
    }
}

export async function createStampImage (body: any, context: Context): Promise<any> {
    console.info('CREATING STAMP IMAGE')
    try {
        // Import the required libraries or packages
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { createCanvas } = require('canvas')
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { registerFont } = require('canvas')
        // Setting the FONT FAMILY
        registerFont('src/resources/' + getFontsFamily('stamp') +'.ttf', { family: 'FontName' })
        //Getting the Company attributes
        const messageBody=body;
        const companyId: number=Number.parseInt(messageBody.data.company.id);
        const companyName: string=messageBody.data.company.name;
        const companyNifCifVal: string=messageBody.data.company.nifCifVal;
        const companyAddress: string=messageBody.data.company.address;
        const companyCity: string=messageBody.data.company.city;
        const companyCountry: string=messageBody.data.company.country;
        const companyZipCode: string=messageBody.data.company.zipCode;
        const companyPhone: string=messageBody.data.company.phone;
        /*
            CREATING THE COMPANY STAMP
        */
        const imgDimensions = getStampImageDimensions().split('x');
        let width: number = Number.parseInt(imgDimensions[0]);
        const height: number = Number.parseInt(imgDimensions[1]);
        const canvas = createCanvas(width, height);
        const ctx = canvas.getContext('2d');
        // Set some styles (Black background)
        ctx.fillStyle = '#fff';
        // Here we create a rectangle
        ctx.fillRect(0,0,width,height);
        // Now we start filling fill the rectangle with the data coming in the event
        // payload and set some styles (black text)
        ctx.font = 'bold 20pt FontName';
        ctx.textAlign = 'center';
        ctx.fillStyle = '#000';
        const gap = 4;
        let yPosition = getNextVerticalAxisPosition(5,0,height,10,gap);
        ctx.fillText(companyName, width/2, yPosition);
        ctx.font = '10pt FontName';
        yPosition = getNextVerticalAxisPosition(5,yPosition,height,10,gap*2);
        ctx.fillText("Reg No: " + companyNifCifVal, width/2, yPosition);
        yPosition = getNextVerticalAxisPosition(5,yPosition,height,10,gap);
        ctx.fillText(companyAddress, width/2, yPosition);
        yPosition = getNextVerticalAxisPosition(5,yPosition,height,10,gap);
        ctx.fillText(companyCity + ', ' + companyCountry + ', ' + companyZipCode, width/2, yPosition);
        yPosition = getNextVerticalAxisPosition(5,yPosition,height,10,gap);
        ctx.fillText("Tel: " + companyPhone, width/2, yPosition);
        //Let's draw a box so teh content gets included in it
        ctx.beginPath();
        const x = width - width*0.98
        const y = height - height*0.95
        ctx.rect(x, y, width*0.98 - x, height*0.95 - y);
        ctx.stroke();
        // Lastly, let's get a buffer from the canvas
        const buffer=canvas.toBuffer("image/png")
        let s3Key = 'companies/' + companyId + 
                        '/stamp.png'
        // and save it to S3
        const uploadParams = {
            Bucket: getS3UsersBucketName(),
            Body: buffer,
            Key: s3Key
        } 
        const s3 = new aws.S3()
        let uploadResults = await s3.upload(uploadParams).promise().then(function (_data: any) {
            return ({status: 'OK', message:'Image generated and uploaded to S3 successfully !!!'})
        }, function (error: any) {
            return ({status: 'ERR', message: error})
        })
        if (uploadResults.status === 'OK') {
            
                const outputMessageBody = {
                    type: OutputEventType.STAMP,
                    subtype: "company",
                    notificationUserId: companyId,
                    notificationCompanyId: companyId,
                    origin: context.functionName,
                    notificationExecutionId: context.awsRequestId,
                    transactionType: TransactionType.EVENT,
                    data: {
                            company: { 
                                id: companyId,
                                stampUrl: s3Key
                            }
                    }
                
                }
                const queueUrl = getCompanyStampGenerationOuputQueueURL();
                await sendOutputMessage(JSON.stringify(outputMessageBody), queueUrl);
                body.data['message']='Image created OK'
        }
        return body
    } catch (e) {
        console.info('Error', e)
        body.data['message']='Error while creating STAMP image ... ' + e
        return body
    }
}

export async function createLogoImage (body: any, context: Context): Promise<any> {
    console.info('CREATING LOGO IMAGE')
    try {
        // Import the required libraries or packages
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { createCanvas } = require('canvas')
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { registerFont } = require('canvas')
        // Setting the FONT FAMILY
        registerFont('src/resources/' + getFontsFamily('logo') +'.ttf', { family: 'FontName' })
        // Get input params from the SQS event payload
        const imgDimensions = getLogoImageDimensions().split('x');
        const width: number = Number.parseInt(imgDimensions[0]);
        const height: number = Number.parseInt(imgDimensions[1]);
        //Getting the Company attributes
        const messageBody=body;
        const companyId: number=Number.parseInt(messageBody.data.company.id);
        const companyName: string=messageBody.data.company.name;
        /*
            CREATING THE COMPANY LOGO
        */
        const canvas = createCanvas(width, height);
        const ctx = canvas.getContext('2d');
        // Set some styles (Black background)
        ctx.fillStyle = '#fff';
        // Here we create a rectangle
        ctx.fillRect(0,0,width,height);
        // Now we start filling fill the rectangle with the data coming in the event
        // payload and set some styles (black text)
        ctx.font = 'bold 30px FontName';
        ctx.textAlign = 'center';
        ctx.fillStyle = '#000';
        ctx.fillText(companyName, width/2, height/2);
            // Lastly, let's get a buffer from the canvas
        const buffer=canvas.toBuffer("image/png");
        const s3Key = 'companies/' + companyId + 
                        '/logo.png'
        // and save it to S3
        const uploadParams = {
            Bucket: getS3UsersBucketName(),
            Body: buffer,
            Key: s3Key
        } 
        const s3 = new aws.S3()
        const uploadResults = await s3.upload(uploadParams).promise().then(function (_data: any) {
                return ({status: 'OK', message:'Image generated and uploaded to S3 successfully !!!'})
        }, function (error: any) {
                return ({status: 'ERR', message: error})
        })
        
        if (uploadResults.status === 'OK') {
            const outputMessageBody = {
                type: OutputEventType.LOGO,
                subtype: "company",
                notificationUserId: companyId,
                notificationCompanyId: companyId,
                origin: context.functionName,
                notificationExecutionId: context.awsRequestId,
                transactionType: TransactionType.EVENT,
                data: {
                            company: { 
                                id: companyId,
                                logoUrl: s3Key
                            }
                }
            }
            const queueUrl = getCompanyLogoGenerationOuputQueueURL();
            await sendOutputMessage(JSON.stringify(outputMessageBody), queueUrl);
            body.data['message']='Image created OK'
        }
        return body
    } catch (e) {
        console.info('Error', e)
        body.data['message']='Error while creating LOGO image ... ' + e
        return body
    }
}
