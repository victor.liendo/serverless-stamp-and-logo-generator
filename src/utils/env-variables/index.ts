/* eslint-disable prettier/prettier */
export function getSignatureImageDimensions(): string {
  return (process.env.USER_SIGNATURE_DIMENSIONS || '400x120')
}

export function getStampImageDimensions(): string {
  return (process.env.COMPANY_STAMP_DIMENSIONS || '400x120')
}

export function getLogoImageDimensions(): string {
  return (process.env.COMPANY_LOGO_DIMENSIONS || '300x120')
}

export function getS3UsersBucketName(): string  {
  return (process.env.S3_BUCKET_USERS || 'UNDEF')
}

export function getFontsFamily(type: string): string {
  let fonts: string = ''
  switch (type) {
    case 'signature' : {
      fonts = (process.env.USER_SIGNATURE_FONTS || 'Satisfy-Regular')
      break
    }
    case 'stamp' : {
      fonts = (process.env.COMPANY_STAMP_LOGO_FONTS || 'Alata-Regular')
      break
    }
    case 'logo' : {
      fonts = (process.env.COMPANY_STAMP_LOGO_FONTS || 'Alata-Regular')
      break
    }
  }
  return fonts
}

export function getUserSignatureGenerationOuputQueueURL(): string {
  return ((process.env.AWS_SQS_USERS_SIGNATURE_OUTPUT_QUEUE_URL) || 'URL')
}

export function getCompanyStampGenerationOuputQueueURL(): string {
  return ((process.env.AWS_SQS_COMPANIES_STAMP_OUTPUT_QUEUE_URL) || 'URL')
}

export function getCompanyLogoGenerationOuputQueueURL(): string {
  return ((process.env.AWS_SQS_COMPANIES_LOGO_OUTPUT_QUEUE_URL) || 'URL')
}