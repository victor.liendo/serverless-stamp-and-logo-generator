/* eslint-disable @typescript-eslint/no-var-requires */


/* eslint-disable prettier/prettier */
const aws = require('aws-sdk');
const sqs = new aws.SQS();
/*
    Sends a message (to the specified QUEUE , with the specified body)
*/

export async function sendOutputMessage(body: string, queueUrl: string): Promise<string> {
    const params = {
        MessageBody: body,
        QueueUrl: queueUrl
    };
    await sqs.sendMessage(params).promise();
    return 'OK';
}
/*
    This function returns the vertical position where the image will
    be filled with some text.

    It can be the height/2 (the middle) if the image will contain
    another image in the first half

    Otherwise, we need to compute it

    here we are with an example of how to call the function:

    getNextVerticalAxisPosition(6,f,0,300,10,4) where:
    linesNumber: number of lines to be inserted
                 startAtTheMiddle: explained above
                 prevPos: 0 of no line has been written yet, otherwise
                          the veritcal position of the previous line
                 300: image height
                 10: size in pixels of the text
                 4: gap between lines
*/

export function getNextVerticalAxisPosition(
                linesNumber: number,
                prevPos: number,
                height: number,
                size: number,
                gap : number,
                ) : number {
    let y: number;             
    if (prevPos === 0) {
        const spaceToBeUsed = size*2 + gap + (linesNumber - 1)*size
                                           + (linesNumber -2)*gap;
        y = (height - spaceToBeUsed)/2 + (size*2) - 5;    
    } else {
        y = prevPos + size + gap;
    }
    return y;
}

export const computeCanvasWidth = (ctx: any, text: string, width: number): number => { 
    return ctx.measureText(text) + (width*0.5)
}