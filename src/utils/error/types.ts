export class NotFoundError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'NotFoundError'
  }
}
export class ConfigurationError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'ConfigurationError'
  }
}
export class ModelError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'ModelError'
  }
}
export class DataAlreadyExists extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'DataAlreadyExists'
  }
}

export class EmptyBodyRequest extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'EmptyBodyRequest'
  }
}
export class ParamError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'ParamError'
  }
}
export class LoginError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'LoginError'
  }
}
