import { ErrorHandler } from '../../@types'
import { APIGatewayProxyResult } from 'aws-lambda'
import {
  NotFoundError,
  ConfigurationError,
  ModelError,
  DataAlreadyExists,
  EmptyBodyRequest,
  LoginError,
  ForbiddenError,
  UnauthorizedError
} from './'
import { ConnectionError } from 'sequelize'
import { AWSError } from 'aws-sdk'
import { ParamError } from './types'

export function errorHandlerDecorator<E extends Error, T>(
  errorHandlers: ErrorHandler<E, Promise<T>>[] = []
) {
  return <F extends (...params: any[]) => Promise<T>>(f: F) => {
    return async (...params: Parameters<F>): Promise<T | Error> => {
      try {
        return await f(...params)
      } catch (error) {
        for (const errorHandler of errorHandlers) {
          if (error instanceof errorHandler.type) {
            return await errorHandler.handler(error)
          }
        }
        return error
      }
    }
  }
}
export function defaultErrorHandlerDecorator<
  F extends (...params: any[]) => Promise<APIGatewayProxyResult>
>(f: F) {
  return async (...params: Parameters<F>): Promise<APIGatewayProxyResult> => {
    const response = await errorHandlerDecorator<
      | NotFoundError
      | ConfigurationError
      | ModelError
      | ParamError
      | DataAlreadyExists
      | EmptyBodyRequest
      | ConnectionError
      | LoginError
      | UnauthorizedError
      | ForbiddenError
      | AWSError,
      APIGatewayProxyResult
    >([
      {
        type: NotFoundError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 404,
          body: JSON.stringify({ message: error.message })
        })
      },
      {
        type: ConfigurationError,
        handler: async (error): Promise<APIGatewayProxyResult> => {
          console.error(error)
          return {
            statusCode: 500,
            body: JSON.stringify({ message: error })
          }
        }
      },
      {
        type: ModelError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 400,
          body: JSON.stringify({ message: error.message })
        })
      },
      {
        type: ParamError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 400,
          body: JSON.stringify({ message: error.message })
        })
      },
      {
        type: DataAlreadyExists,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 400,
          body: JSON.stringify({ message: error.message })
        })
      },
      {
        type: EmptyBodyRequest,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 400,
          body: JSON.stringify({ message: error.message })
        })
      },
      {
        type: ConnectionError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 500,
          body: JSON.stringify({
            message:
              'Unable to connect to database, check network problems or configurations',
            error: error
          })
        })
      },
      {
        type: LoginError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 500,
          body: JSON.stringify({ message: 'Unknown error while login', error })
        })
      },
      {
        type: UnauthorizedError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 401,
          body: JSON.stringify({ message: error.message, error })
        })
      },
      {
        type: ForbiddenError,
        handler: async (error): Promise<APIGatewayProxyResult> => ({
          statusCode: 403,
          body: JSON.stringify({ message: error.message, error })
        })
      },
      {
        // Handle AWS Error. AWS Errors is an abstract clase so doesn't have constructor. it should be got by error
        type: Error,
        handler: async (error): Promise<APIGatewayProxyResult> => {
          const awsError = error as AWSError
          const statusCode =
            awsError.code === 'NotAuthorizedException'
              ? 401
              : awsError.code === 'InvalidPasswordException'
              ? 400
              : 500

          return {
            statusCode,
            body: JSON.stringify({
              message: awsError.message,
              code: awsError.code,
              region: awsError.region,
              requestId: awsError.requestId,
              cfId: awsError.cfId,
              hostName: awsError.hostname
            })
          }
        }
      }
    ])(f)(...params)
    return response instanceof Error
      ? {
          statusCode: 500,
          body: JSON.stringify({ message: 'Unhandled Error', error: response })
        }
      : response
  }
}
