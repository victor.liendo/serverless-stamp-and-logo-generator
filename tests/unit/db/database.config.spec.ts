import 'mocha'
import { expect } from 'chai'
import { getDBType } from '../../../src/db/database.config'
import sinon from 'sinon'
describe('Get DB type', () => {
  it('Should return the right DB', () => {
    const expected = 'postgres'
    const obtained = getDBType(expected)
    expect(obtained).to.be.equal(expected)
  })
  it('Should return a default DB type', () => {
    const expected = 'mysql'
    const obtained = getDBType()
    expect(obtained).to.be.equal(expected)
  })
  it('Should throw error if types does not match', () => {
    const getDBTypeSpy = sinon.spy(getDBType)
    try {
      getDBTypeSpy('test')
    } catch (error) {}
    expect(getDBTypeSpy.threw()).to.be.true
  })
})
