import 'mocha'
import { expect } from 'chai'
import { connectFactory } from '../../../src/db'
import sinon, { SinonSpy } from 'sinon'
import { Sequelize } from 'sequelize'
import { SequelizeSetUp } from '../../../src/@types'
import Sinon from 'sinon'

describe('Database Connection', () => {
  const mockConfig: SequelizeSetUp = {
    database: 'test',
    host: 'test',
    username: 'test',
    password: '',
    dialect: 'mysql',
    port: 0
  }
  let mockDB: Sequelize
  let sync: Sinon.SinonSpy
  let authenticate: SinonSpy
  let value: boolean
  let mockDBConnect: () => Promise<boolean>

  beforeEach(async () => {
    mockDB = new Sequelize(mockConfig)
    sync = sinon.spy()
    authenticate = sinon.spy()
    mockDB.sync = sync
    mockDB.authenticate = authenticate
    mockDBConnect = connectFactory(mockDB)
    value = await mockDBConnect()
  })

  it('Should return true if a connection exists', async function() {
    expect(value).to.be.true
  })

  it('Should call to connect the databas', async () => {
    expect(sync.calledOnce).to.be.true
  })

  it('Should call sync function', async () => {
    expect(authenticate.calledOnce).to.be.true
  })

  it('Should not make a new connection on the same DB', async () => {
    await mockDBConnect()
    const lastState = await mockDBConnect()
    expect(sync.calledOnce).to.be.true
    expect(authenticate.calledOnce).to.be.true
    expect(lastState).to.be.true
  })
})
